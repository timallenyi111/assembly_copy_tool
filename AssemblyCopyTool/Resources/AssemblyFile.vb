﻿Imports Inventor
Imports System.Windows.Forms
Public Class AssemblyFile
    Private oAsyName As String
    Private nAsyName As String
    Private oFullAsyName As String
    Private nFullAsyName As String = vbNull
    Private oPartNumber As String
    Private nPartNumber As String
    Private compDoc As AssemblyDocument
    Private partList As New ArrayList
    Private subAsyList As New ArrayList
    Private parentAsyList As New ArrayList
    Private compDef As AssemblyComponentDefinition
    Private tNode1 As TreeNode
    Private tNode2 As TreeNode

    Property AssemblyDocument As AssemblyDocument
        Get
            Return compDoc
        End Get
        Set(value As AssemblyDocument)
            compDoc = value
        End Set
    End Property

    Property AssemblyComponentDefinition As AssemblyComponentDefinition
        Get
            Return compDef
        End Get
        Set(value As AssemblyComponentDefinition)
            compDef = value
        End Set
    End Property

    Property OriginalName As String
        Get
            Return oAsyName
        End Get
        Set(value As String)
            oAsyName = value
        End Set
    End Property

    Property NewName As String
        Get
            Return nAsyName
        End Get
        Set(value As String)
            nAsyName = value
        End Set
    End Property

    Property OriginalFullFileName As String
        Get
            Return oFullAsyName
        End Get
        Set(value As String)
            oFullAsyName = value
        End Set
    End Property

    Property NewFullFileName As String
        Get
            Return nFullAsyName
        End Get
        Set(value As String)
            nFullAsyName = value
        End Set
    End Property

    Property OriginalPartNumber As String
        Get
            Return oPartNumber
        End Get
        Set(value As String)
            oPartNumber = value
        End Set
    End Property

    Property NewPartNumber As String
        Get
            Return nPartNumber
        End Get
        Set(value As String)
            nPartNumber = value
        End Set
    End Property

    Property TreeNode1 As TreeNode
        Get
            Return tNode1
        End Get
        Set(value As TreeNode)
            tNode1 = value
        End Set
    End Property

    Property TreeNode2 As TreeNode
        Get
            Return tNode2
        End Get
        Set(value As TreeNode)
            tNode2 = value
        End Set
    End Property

    ReadOnly Property Parts() As ArrayList
        Get
            Return partList
        End Get
    End Property

    ReadOnly Property SubAssemblys() As ArrayList
        Get
            Return subAsyList
        End Get
    End Property

    ReadOnly Property ParentAssemblys() As ArrayList
        Get
            Return parentAsyList
        End Get
    End Property

    'checks for duplicate parts returns true if the part is a duplicate
    Public Function CheckDuplicatePart(ByVal part As PartDocument) As Boolean
        Dim dupCheck As Boolean = False

        For Each curPart As PartFile In partList
            If part.DisplayName.ToString = curPart.OriginalName Then
                dupCheck = True
            End If
        Next
        Return dupCheck
    End Function

    'checks for duplicate sub assemblys returns true if the assembly is a duplicate
    Public Function CheckDuplicateAsy(ByVal asm As AssemblyDocument) As Boolean
        Dim dupCheck2 As Boolean = False

        For Each curAsm As AssemblyFile In subAsyList
            If asm.DisplayName.ToString = curAsm.OriginalName Then
                dupCheck2 = True
            End If
        Next

        Return dupCheck2

    End Function

    Public Function AddSubAssembly(ByVal subAsy As AssemblyFile) As Object
        subAsyList.Add(subAsy)
        Return vbNull
    End Function

    Public Function AddPart(ByVal part As PartFile) As Object
        partList.Add(part)
        Return vbNull
    End Function

    Public Function AddParentAssembly(ByVal parAsy As AssemblyFile) As Object
        parentAsyList.Add(parAsy)
        Return vbNull
    End Function

End Class
