﻿Imports System.Activator
Imports System.Runtime.InteropServices
Imports System.String
Imports System.Type
Imports System.Windows.Forms
Imports Inventor
Imports Microsoft.VisualBasic.Strings

Public Class AsyCopyForm
    Dim _invApp As Inventor.Application = Marshal.GetActiveObject("Inventor.Application")
    Dim _fileManager As FileManager = _invApp.FileManager
    Dim _oAsmDoc As AssemblyDocument = _invApp.ActiveDocument
    Dim _oAsmCompDef As AssemblyComponentDefinition = _oAsmDoc.ComponentDefinition
    Dim originalFilePath As String = _oAsmDoc.FullFileName.ToString
    Dim originalFullFileName As String = Microsoft.VisualBasic.Strings.Left(originalFullFileName, InStrRev(_oAsmDoc.FullFileName.ToString, "\"))
    Dim _parentAsy As New AssemblyFile


    Private Function ComponentSetup(Optional ByVal asyFile As AssemblyFile = Nothing) As AssemblyFile
        Dim partNode As New TreeNode
        Dim subAsmNode As New TreeNode
        Dim subAsm As New AssemblyFile
        Dim masterPartList As New ArrayList()
        Dim masterAsyList As New ArrayList()


        Dim activeProject As DesignProject = _invApp.DesignProjectManager.ActiveDesignProject
        Dim currentProjectPath As String = activeProject.FullFileName.Substring(0, InStr(activeProject.FullFileName, activeProject.Name) - 1)
        Dim currentAssemblyName As String = _oAsmDoc.DisplayName.Substring(0, Len(_oAsmDoc.DisplayName) - 4)
        newDirectoryTB.Text = currentProjectPath & PrefixTB.Text & currentAssemblyName & SuffixTB.Text

        Dim newFilePath As String = newDirectoryTB.Text & "\"


        'Setting up main assembly object        

        'When the function is ran with no Assembly File, it means that it is the first iteration and is the master parent assembly
        'When sub assemblys are passed into the function, they will come with an Assembly File
        If asyFile Is Nothing Then
            'if there are no parts or sub assemblys in the parent assembly, it means that the parent assembly has not been set up yet because its the first run
            'A new assembly file should be made
            'The reset flag is here 
            If _parentAsy.Parts.Count = 0 And _parentAsy.SubAssemblys.Count = 0 Then
                asyFile = AsyObjectInitialSetup(_oAsmDoc, _oAsmCompDef, newFilePath)
            Else
                asyFile = _parentAsy
            End If

        End If

        'Stepping through parent assembly and setting up parts and sub assemblys initial parameters
        For Each curComp As ComponentOccurrence In asyFile.AssemblyComponentDefinition.Occurrences

            If curComp.Suppressed = False Then
                'checks if a component is a part or an assembly
                If curComp.DefinitionDocumentType = DocumentTypeEnum.kPartDocumentObject Then

                    Dim curPart As PartDocument = curComp.Definition.Document
                    Dim curPartDef As PartComponentDefinition = curComp.Definition
                    Dim newPartFile As New PartFile

                    'checks if a part is a duplicate in the main assembly
                    If asyFile.CheckDuplicatePart(curPart) Then

                    Else

                        newPartFile = PartObjectInitialSetup(curPart, curPartDef, asyFile, newFilePath,)
                        asyFile.AddPart(newPartFile)
                    End If

                    'if the component is an assembly, pass it to the sub assembly function to be stepped through
                Else

                    'subAsm = SubAssemblySetup(curComp.Definition, parentAsy, newFilePath)
                    Dim subAsmCompDef As AssemblyComponentDefinition = curComp.Definition
                    Dim subAsmDoc As AssemblyDocument = subAsmCompDef.Document

                    subAsm = AsyObjectInitialSetup(subAsmDoc, subAsmCompDef, newFilePath, asyFile)

                    asyFile.AddSubAssembly(subAsm)

                    ComponentSetup(subAsm)

                End If
            End If

        Next

        'set the master parent assembly file as the asyFile that has been through this loop
        'should only exit the loop after stepping through everything in the parent assemly
        _parentAsy = asyFile

        Return _parentAsy
    End Function

    Private Function AddTabs(inString As String, tabCount As Integer) As String
        Dim outString As String = ""
        Dim x As Integer = 0

        If tabCount = 0 Then
            outString = inString
        Else
            While x < tabCount
                outString = outString & "   "
                x += 1
            End While
            outString = outString & inString
        End If

        Return outString

    End Function

    Private Function WriteSubAsyTXT(subAsy As AssemblyFile, tabCount As Integer, oFile As System.IO.StreamWriter) As Object

        tabCount += 1

        For Each subPart As PartFile In subAsy.Parts
            oFile.WriteLine(AddTabs(subPart.NewFullFileName, tabCount))
        Next

        For Each curAsy As AssemblyFile In subAsy.SubAssemblys
            oFile.WriteLine(AddTabs(curAsy.NewFullFileName, tabCount))
            WriteSubAsyTXT(curAsy, tabCount, oFile)
        Next

        tabCount -= 1

        Return vbNull
    End Function

    'this function will step through sub assemblys
    'takes the sub assembly component definition and treenode
    'can be called within it self for sub assemblys inside sub assemblys

    'sets all properties for parts when the form is first loaded
    Private Function PartObjectInitialSetup(curPart As PartDocument, curDef As PartComponentDefinition, parentAsy As AssemblyFile, newPath As String,
            Optional ByVal parentNode1 As TreeNode = Nothing, Optional ByVal parentNode2 As TreeNode = Nothing) As PartFile

        Dim newPart As New PartFile


        newPart.OriginalFullFileName = curPart.FullFileName.ToString
        newPart.OriginalName = RemovePath(curPart.FullFileName.ToString)
        newPart.OriginalPartNumber = curPart.PropertySets.Item("Design Tracking Properties").ItemByPropId(5).Value

        newPart.NewPartNumber = AddPrefixSuffix(newPart.OriginalPartNumber, PrefixTB.Text, SuffixTB.Text)
        newPart.NewName = Concat(newPart.NewPartNumber, ".ipt")
        newPart.NewFullFileName = Concat(newPath, newPart.NewName)

        newPart.PartDocument = curPart
        newPart.PartComponentDefinition = curDef
        newPart.AddParentAssembly(parentAsy)

        If parentAsy Is Nothing Then
            'Console.WriteLine("setting initial treeview node")
            TreeView1.Nodes.Add(newPart.OriginalPartNumber)
            TreeView2.Nodes.Add(newPart.NewPartNumber)
        Else
            'Console.WriteLine("setting up additional node")
            parentAsy.TreeNode1.Nodes.Add(newPart.OriginalPartNumber)
            parentAsy.TreeNode2.Nodes.Add(newPart.NewPartNumber)
        End If

        Return newPart



    End Function

    'sets all properties for assemblys when the form is first loaded
    Private Function AsyObjectInitialSetup(curAsy As AssemblyDocument, curDef As AssemblyComponentDefinition, newPath As String,
               Optional ByVal pAsy As AssemblyFile = Nothing) As AssemblyFile

        Dim newAsy As New AssemblyFile
        Dim pNode1 As New TreeNode
        Dim pNode2 As New TreeNode

        newAsy.OriginalFullFileName = curAsy.FullFileName.ToString
        newAsy.OriginalName = RemovePath(curAsy.FullFileName.ToString)
        newAsy.OriginalPartNumber = curAsy.PropertySets.Item("Design Tracking Properties").ItemByPropId(5).Value

        newAsy.NewPartNumber = AddPrefixSuffix(newAsy.OriginalPartNumber, PrefixTB.Text, SuffixTB.Text)
        newAsy.NewName = Concat(newAsy.NewPartNumber, ".iam")
        newAsy.NewFullFileName = Concat(newPath, newAsy.NewName)

        newAsy.AssemblyDocument = curAsy
        newAsy.AssemblyComponentDefinition = curDef

        If pAsy Is Nothing Then
            newAsy.TreeNode1 = TreeView1.Nodes.Add(newAsy.OriginalPartNumber)
            newAsy.TreeNode2 = TreeView2.Nodes.Add(newAsy.NewPartNumber)
        Else
            newAsy.AddParentAssembly(pAsy)
            newAsy.TreeNode1 = pAsy.TreeNode1.Nodes.Add(newAsy.OriginalPartNumber)
            newAsy.TreeNode2 = pAsy.TreeNode2.Nodes.Add(newAsy.NewPartNumber)
        End If

        Return newAsy

    End Function

    Private Function AddPrefixSuffix(name As String, prefix As String, suffix As String) As String

        Return Concat(prefix, name, suffix)

    End Function

    'removes the path from the full file name to return only the file name
    Private Function RemovePath(fullFileName As String) As String
        Dim pathOnly As String
        Dim nameOnly As String
        pathOnly = Microsoft.VisualBasic.Strings.Left(fullFileName, InStrRev(fullFileName, "\"))
        'Console.WriteLine(pathOnly)

        nameOnly = fullFileName.Replace(pathOnly, "")
        Return nameOnly

    End Function

    Private Function SaveFiles(Optional ByVal asmFile As AssemblyFile = Nothing) As Object

        'asmFile will be nothing when the first loop is ran
        If asmFile Is Nothing Then
            asmFile = _parentAsy
            'Console.WriteLine("")
            'Console.WriteLine("")
            'Console.WriteLine("")
            'Console.WriteLine(Concat("saving parent assembly: ", asmFile.NewFullFileName))



            Dim curAsmDef As AssemblyComponentDefinition = asmFile.AssemblyComponentDefinition
            Dim curAsmDoc As AssemblyDocument = asmFile.AssemblyDocument
            'save the parent assembly

            If My.Computer.FileSystem.DirectoryExists(newDirectoryTB.Text) Then

            Else
                My.Computer.FileSystem.CreateDirectory(newDirectoryTB.Text)
            End If


            If My.Computer.FileSystem.FileExists(asmFile.NewFullFileName) Then
                My.Computer.FileSystem.DeleteFile(asmFile.NewFullFileName)
                curAsmDoc.SaveAs(asmFile.NewFullFileName, False)
            Else
                curAsmDoc.SaveAs(asmFile.NewFullFileName, False)
            End If
        Else
            'Dim curAsmDoc As AssemblyDocument = asmFile.AssemblyDocument

        End If

        DebugLabel.Text = "Saving Files For: " & asmFile.NewName

        For Each curPart As PartFile In asmFile.Parts
            Dim nPart As PartDocument
            nPart = curPart.PartDocument
            Console.WriteLine(Concat("saving part: ", curPart.NewFullFileName))



            If My.Computer.FileSystem.FileExists(curPart.NewFullFileName) Then

            Else
                Try
                    My.Computer.FileSystem.CopyFile(curPart.OriginalFullFileName, curPart.NewFullFileName, True)
                Catch ex As Exception
                    MsgBox(curPart.OriginalFullFileName)
                End Try

            End If
        Next

        For Each curAsy As AssemblyFile In asmFile.SubAssemblys
            'step back into save function with this assembly
            Console.WriteLine(Concat("Saving Assembly: ", curAsy.NewFullFileName))
            'save the sub assembly that is currently being worked on
            If My.Computer.FileSystem.FileExists(curAsy.NewFullFileName) Then

            Else
                My.Computer.FileSystem.CopyFile(curAsy.OriginalFullFileName, curAsy.NewFullFileName)
            End If

            Dim curAsyDoc As AssemblyDocument = curAsy.AssemblyDocument
            Dim curAsyDef As AssemblyComponentDefinition = curAsy.AssemblyComponentDefinition

            SaveFiles(curAsy)
        Next



        Return vbNull
    End Function

    Private Function ReplaceFiles(Optional asmFile As AssemblyFile = Nothing) As Object

        Dim compOcc As ComponentOccurrences
        Dim mAsyDoc As AssemblyDocument = _invApp.ActiveDocument
        Dim activeDoc As AssemblyDocument



        'asmFile will be nothing on the first pass through
        If asmFile Is Nothing Then
            'refresh the main active component definition
            _oAsmDoc = _invApp.ActiveDocument
            _oAsmCompDef = _oAsmDoc.ComponentDefinition
            asmFile = _parentAsy
            compOcc = _oAsmCompDef.Occurrences
        Else
            compOcc = mAsyDoc.ComponentDefinition.Occurrences
        End If

        'Console.WriteLine(vbCrLf & vbCrLf & "Start Replacing Files in: " & asmFile.OriginalPartNumber)

        DebugLabel.Text = "Replacing Files In: " & asmFile.NewName

        'gets the list of components in the specified document. 
        For Each curOcc As ComponentOccurrence In compOcc

            'Console.WriteLine(vbCrLf & "replacing " & curOcc._DisplayName.ToString & " in " & asmFile.NewPartNumber)

            If curOcc.Suppressed = False Then

                'if the component is a part
                If curOcc.DefinitionDocumentType = DocumentTypeEnum.kPartDocumentObject Then

                    Dim curPartDoc As PartDocument = curOcc.Definition.Document

                    'search through the list of parts in the assembly file until a match is found
                    For Each curPart As PartFile In asmFile.Parts
                        'Console.WriteLine("Trying part: " & curPart.OriginalPartNumber)
                        If curPart.OriginalName = RemovePath(curPartDoc.FullFileName) Then
                            'Console.WriteLine("replacing: " & curPart.OriginalName & " with: " & RemovePath(curPartDoc.FullFileName))
                            'Console.WriteLine("**match found**")
                            curOcc.Replace(curPart.NewFullFileName, True)
                            Exit For
                        Else

                        End If
                    Next


                    'if the component is an assembly
                Else

                    'search through sub assemblys for a match
                    For Each curAsy As AssemblyFile In asmFile.SubAssemblys
                        Dim curAsyDoc As AssemblyDocument = curOcc.Definition.Document

                        'Console.WriteLine("Trying Assembly: " & curAsy.OriginalPartNumber)

                        If curAsy.OriginalName = RemovePath(curAsyDoc.FullFileName) Then

                            'Console.WriteLine("**match found**")

                            curOcc.Replace(curAsy.NewFullFileName, True)

                            activeDoc = _invApp.Documents.Open(curAsy.NewFullFileName)

                            ReplaceFiles(curAsy)

                            activeDoc.Save()
                            activeDoc.Close()

                            Exit For
                        Else
                            Console.WriteLine(curAsy.OriginalName & " is not a match for " & RemovePath(mAsyDoc.FullDocumentName))
                        End If

                    Next

                End If

            End If

            Console.WriteLine("done replacing files")

        Next


        Return Nothing
    End Function

    Private Function ReplacePartNumbers(Optional ByVal cAsy As AssemblyFile = Nothing) As Object
        Dim replacePartDoc As PartDocument
        Dim replaceAsyDoc As AssemblyDocument

        If cAsy Is Nothing Then
            cAsy = _parentAsy
            replaceAsyDoc = _oAsmDoc
            _oAsmDoc.PropertySets.Item("Design Tracking Properties").ItemByPropId(5).Value = cAsy.NewPartNumber
        End If

        DebugLabel.Text = "Updating Part Numbers In: " & cAsy.NewName

        For Each curPart As PartFile In cAsy.Parts
            replacePartDoc = _invApp.Documents.ItemByName(curPart.NewFullFileName)
            replacePartDoc.PropertySets.Item("Design Tracking Properties").ItemByPropId(5).Value = curPart.NewPartNumber
        Next

        For Each curAsy As AssemblyFile In cAsy.SubAssemblys
            replaceAsyDoc = _invApp.Documents.ItemByName(curAsy.NewFullFileName)
            replaceAsyDoc.PropertySets.Item("Design Tracking Properties").ItemByPropId(5).Value = curAsy.NewPartNumber
            ReplacePartNumbers(curAsy)
        Next

        Return vbNull
    End Function



#Region "Form Controls"

    Private Sub AsyCopyForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        'Trys to start inventor if it is not already running. 
        Dim _started As Boolean = False
        Try

        Catch ex As Exception
            Try
                Dim invAppType As Type = GetTypeFromProgID("Inventor.Application")
                _invApp = CreateInstance(invAppType)
                _invApp.Visible = True
                _started = True
            Catch ex2 As Exception
                MsgBox(ex2.ToString())
                MsgBox("Unable to get or start Inventor")
            End Try
        End Try

        'set the default text/file path to the current project directory
        'Dim activeProject As DesignProject = _invApp.DesignProjectManager.ActiveDesignProject
        'Dim currentProjectPath As String = activeProject.FullFileName.Substring(0, InStr(activeProject.FullFileName, activeProject.Name) + Len(activeProject.Name) - 1)
        'Dim currentAssemblyName As String = _oAsmDoc.DisplayName.Substring(0, Len(_oAsmDoc.DisplayName) - 4)
        'newDirectoryTB.Text = currentProjectPath & "\" & PrefixTB.Text & currentAssemblyName & SuffixTB.Text
        'DebugLabel.Text = currentProjectPath & "\" & currentAssemblyName & SuffixTB.Text
        ComponentSetup()
    End Sub

    Private Sub exitButton_Click(sender As Object, e As EventArgs) Handles exitButton.Click
        Close()
    End Sub

    Private Sub PrefixTB_Leave(sender As Object, e As EventArgs) Handles PrefixTB.Leave
        TreeView1.Nodes.Clear()
        TreeView2.Nodes.Clear()
        '''''''''
        'clears all parts and sub assemblys from master parent to trick the component setup into resetting all data
        'not a good way to do this
        _parentAsy.SubAssemblys.Clear()
        _parentAsy.Parts.Clear()

        ComponentSetup()
    End Sub

    Private Sub SuffixTB_Leave(sender As Object, e As EventArgs) Handles SuffixTB.Leave
        'Console.WriteLine(vbCrLf & "updating treeview")
        TreeView1.Nodes.Clear()
        TreeView2.Nodes.Clear()
        '''''''''
        'clears all parts and sub assemblys from master parent to trick the component setup into resetting all data
        'not a good way to do this
        _parentAsy.SubAssemblys.Clear()
        _parentAsy.Parts.Clear()

        ComponentSetup()
    End Sub

    Private Sub newDirectoryTB_TextChanged(sender As Object, e As EventArgs) Handles newDirectoryTB.TextChanged
        TreeView1.Nodes.Clear()
        TreeView2.Nodes.Clear()
        Console.WriteLine("setting new save path")
        '''''''''
        'clears all parts and sub assemblys from master parent to trick the component setup into resetting all data
        'not a good way to do this
        _parentAsy.SubAssemblys.Clear()
        _parentAsy.Parts.Clear()
        ComponentSetup()
    End Sub

    Private Sub CopyButton_Click(sender As Object, e As EventArgs) Handles CopyButton.Click
        TreeView1.Nodes.Clear()
        TreeView2.Nodes.Clear()

        SaveFiles()
        Console.WriteLine(vbCrLf & "Done Saving File!")
        ReplaceFiles()
        Console.WriteLine(vbCrLf & "Done Replacing Files")
        '_invApp.Documents.Open(parentAsy.NewFullFileName).Activate()
        ReplacePartNumbers()
        MsgBox("Done Copying")
        Close()
    End Sub

    Private Sub NewDirBut_Click(sender As Object, e As EventArgs) Handles NewDirBut.Click
        'FolderBrowserDialog1.SelectedPath = newDirectoryTB.Text.ToString
        FolderBrowserDialog1.ShowDialog()
        newDirectoryTB.Text = FolderBrowserDialog1.SelectedPath
    End Sub

#End Region

#Region "Move Form"
    Dim MoveForm As Boolean
    Dim MoveForm_MousePosition As Drawing.Point



    Private Sub AsyCopyForm_MouseDown(sender As Object, e As MouseEventArgs) Handles MyBase.MouseDown
        If e.Button = MouseButtons.Left Then
            MoveForm = True
            Me.Cursor = Cursors.NoMove2D
            MoveForm_MousePosition = e.Location
        End If
    End Sub

    Private Sub AsyCopyForm_MouseMove(sender As Object, e As MouseEventArgs) Handles MyBase.MouseMove
        If MoveForm Then
            Me.Location = Me.Location + (e.Location - MoveForm_MousePosition)
        End If
    End Sub

    Private Sub AsyCopyForm_MouseUp(sender As Object, e As MouseEventArgs) Handles MyBase.MouseUp
        If e.Button = MouseButtons.Left Then
            MoveForm = False
            Me.Cursor = Cursors.Default
        End If
    End Sub



#End Region

End Class
