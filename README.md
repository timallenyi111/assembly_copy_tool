README
========

## Install

To get Inventor to recognize this addin move **AssemblyCopyTool.bundle** to the following path ** C:\Users\$username$\AppData\Roaming\Autodesk\ApplicationPlugins\ **

## Update

Overwrite old ** AssemblyCopyTool.bundle ** files with the updated ones.

## Uninstall

Delete ** AssemblyCopyTool.bundle ** Directory from ** C:\Users\$username$\AppData\Roaming\Autodesk\ApplicationPlugins\ **

## V0.1.2 

### Bug Fixes

1. Part Numbers are now updating

### Updates
1. A new directory is now created in the current project and is named after the new assembly name. This directory is no longer editable.

### Known Issues
1. Frame Generator assembly skeletons can not be updated
2. Bolted Connections and other content center parts are copying to the new directory
3. iParts are not copying their iPart factory reference
