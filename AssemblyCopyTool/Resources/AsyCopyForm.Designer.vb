﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class AsyCopyForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(AsyCopyForm))
        Me.TreeView1 = New System.Windows.Forms.TreeView()
        Me.SuffixTB = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.NewDirBut = New System.Windows.Forms.Button()
        Me.newDirectoryTB = New System.Windows.Forms.TextBox()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.FolderBrowserDialog1 = New System.Windows.Forms.FolderBrowserDialog()
        Me.CopyButton = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.PrefixTB = New System.Windows.Forms.TextBox()
        Me.TreeView2 = New System.Windows.Forms.TreeView()
        Me.exitButton = New System.Windows.Forms.Button()
        Me.VersionLable = New System.Windows.Forms.Label()
        Me.DebugLabel = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'TreeView1
        '
        Me.TreeView1.BackColor = System.Drawing.Color.DimGray
        Me.TreeView1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TreeView1.ForeColor = System.Drawing.Color.White
        Me.TreeView1.Location = New System.Drawing.Point(24, 224)
        Me.TreeView1.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.TreeView1.Name = "TreeView1"
        Me.TreeView1.Size = New System.Drawing.Size(231, 300)
        Me.TreeView1.TabIndex = 0
        '
        'SuffixTB
        '
        Me.SuffixTB.BackColor = System.Drawing.Color.DimGray
        Me.SuffixTB.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.SuffixTB.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SuffixTB.ForeColor = System.Drawing.Color.White
        Me.SuffixTB.ImeMode = System.Windows.Forms.ImeMode.[On]
        Me.SuffixTB.Location = New System.Drawing.Point(285, 123)
        Me.SuffixTB.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.SuffixTB.Name = "SuffixTB"
        Me.SuffixTB.Size = New System.Drawing.Size(194, 25)
        Me.SuffixTB.TabIndex = 1
        Me.SuffixTB.Text = "_1"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(118, 126)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(148, 17)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "New Assembly Suffix:"
        '
        'NewDirBut
        '
        Me.NewDirBut.BackColor = System.Drawing.Color.DimGray
        Me.NewDirBut.Enabled = False
        Me.NewDirBut.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.NewDirBut.ForeColor = System.Drawing.Color.White
        Me.NewDirBut.Location = New System.Drawing.Point(30, 37)
        Me.NewDirBut.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.NewDirBut.Name = "NewDirBut"
        Me.NewDirBut.Size = New System.Drawing.Size(120, 30)
        Me.NewDirBut.TabIndex = 3
        Me.NewDirBut.Text = "New Directory"
        Me.NewDirBut.UseVisualStyleBackColor = False
        '
        'newDirectoryTB
        '
        Me.newDirectoryTB.BackColor = System.Drawing.Color.DimGray
        Me.newDirectoryTB.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.newDirectoryTB.ForeColor = System.Drawing.Color.White
        Me.newDirectoryTB.Location = New System.Drawing.Point(164, 40)
        Me.newDirectoryTB.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.newDirectoryTB.Name = "newDirectoryTB"
        Me.newDirectoryTB.Size = New System.Drawing.Size(395, 25)
        Me.newDirectoryTB.TabIndex = 4
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        '
        'CopyButton
        '
        Me.CopyButton.BackColor = System.Drawing.Color.DimGray
        Me.CopyButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.CopyButton.Font = New System.Drawing.Font("Arial", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CopyButton.ForeColor = System.Drawing.Color.White
        Me.CopyButton.Location = New System.Drawing.Point(216, 160)
        Me.CopyButton.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.CopyButton.Name = "CopyButton"
        Me.CopyButton.Size = New System.Drawing.Size(150, 50)
        Me.CopyButton.TabIndex = 6
        Me.CopyButton.Text = "COPY"
        Me.CopyButton.UseVisualStyleBackColor = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.White
        Me.Label2.Location = New System.Drawing.Point(118, 90)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(149, 17)
        Me.Label2.TabIndex = 7
        Me.Label2.Text = "New Assembly Prefix:"
        '
        'PrefixTB
        '
        Me.PrefixTB.BackColor = System.Drawing.Color.DimGray
        Me.PrefixTB.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PrefixTB.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PrefixTB.ForeColor = System.Drawing.Color.White
        Me.PrefixTB.ImeMode = System.Windows.Forms.ImeMode.[On]
        Me.PrefixTB.Location = New System.Drawing.Point(286, 87)
        Me.PrefixTB.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.PrefixTB.Name = "PrefixTB"
        Me.PrefixTB.Size = New System.Drawing.Size(194, 25)
        Me.PrefixTB.TabIndex = 8
        '
        'TreeView2
        '
        Me.TreeView2.BackColor = System.Drawing.Color.DimGray
        Me.TreeView2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TreeView2.ForeColor = System.Drawing.Color.White
        Me.TreeView2.Location = New System.Drawing.Point(328, 224)
        Me.TreeView2.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.TreeView2.Name = "TreeView2"
        Me.TreeView2.Size = New System.Drawing.Size(231, 300)
        Me.TreeView2.TabIndex = 9
        '
        'exitButton
        '
        Me.exitButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(34, Byte), Integer), CType(CType(41, Byte), Integer), CType(CType(51, Byte), Integer))
        Me.exitButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.exitButton.ForeColor = System.Drawing.Color.White
        Me.exitButton.Location = New System.Drawing.Point(560, 0)
        Me.exitButton.Name = "exitButton"
        Me.exitButton.Size = New System.Drawing.Size(24, 24)
        Me.exitButton.TabIndex = 10
        Me.exitButton.Text = "X"
        Me.exitButton.UseMnemonic = False
        Me.exitButton.UseVisualStyleBackColor = False
        '
        'VersionLable
        '
        Me.VersionLable.AutoSize = True
        Me.VersionLable.ForeColor = System.Drawing.Color.White
        Me.VersionLable.Location = New System.Drawing.Point(0, 0)
        Me.VersionLable.Name = "VersionLable"
        Me.VersionLable.Size = New System.Drawing.Size(192, 17)
        Me.VersionLable.TabIndex = 11
        Me.VersionLable.Text = "Assembly Copy Tool - v0.1.2"
        '
        'DebugLabel
        '
        Me.DebugLabel.AutoSize = True
        Me.DebugLabel.ForeColor = System.Drawing.Color.White
        Me.DebugLabel.Location = New System.Drawing.Point(0, 528)
        Me.DebugLabel.Name = "DebugLabel"
        Me.DebugLabel.Size = New System.Drawing.Size(0, 17)
        Me.DebugLabel.TabIndex = 12
        '
        'AsyCopyForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(59, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(83, Byte), Integer))
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.ClientSize = New System.Drawing.Size(584, 545)
        Me.Controls.Add(Me.DebugLabel)
        Me.Controls.Add(Me.VersionLable)
        Me.Controls.Add(Me.exitButton)
        Me.Controls.Add(Me.TreeView2)
        Me.Controls.Add(Me.PrefixTB)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.CopyButton)
        Me.Controls.Add(Me.newDirectoryTB)
        Me.Controls.Add(Me.NewDirBut)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.SuffixTB)
        Me.Controls.Add(Me.TreeView1)
        Me.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ForeColor = System.Drawing.Color.Black
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.MaximizeBox = False
        Me.Name = "AsyCopyForm"
        Me.Text = "Assembly Copy Tool - v0.1"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents TreeView1 As Windows.Forms.TreeView
    Friend WithEvents SuffixTB As Windows.Forms.TextBox
    Friend WithEvents Label1 As Windows.Forms.Label
    Friend WithEvents NewDirBut As Windows.Forms.Button
    Friend WithEvents newDirectoryTB As Windows.Forms.TextBox
    Friend WithEvents OpenFileDialog1 As Windows.Forms.OpenFileDialog
    Friend WithEvents FolderBrowserDialog1 As Windows.Forms.FolderBrowserDialog
    Friend WithEvents CopyButton As Windows.Forms.Button
    Friend WithEvents Label2 As Windows.Forms.Label
    Friend WithEvents PrefixTB As Windows.Forms.TextBox
    Friend WithEvents TreeView2 As Windows.Forms.TreeView
    Friend WithEvents exitButton As Windows.Forms.Button
    Friend WithEvents VersionLable As Windows.Forms.Label
    Friend WithEvents DebugLabel As Windows.Forms.Label
End Class
