﻿Imports Inventor
Public Class PartFile
    Private oPartName As String
    Private nPartName As String
    Private oPartFullFileName As String
    Private nPartFullFileName As String
    Private oPartNumber As String
    Private nPartNumber As String
    Private partDoc As PartDocument
    Private parentAsys As New ArrayList
    Private nameOver As Boolean = False
    Private partDef As PartComponentDefinition

    Property PartDocument As PartDocument
        Get
            Return partDoc
        End Get
        Set(value As PartDocument)
            partDoc = value
        End Set
    End Property

    Property PartComponentDefinition As PartComponentDefinition
        Get
            Return partDef
        End Get
        Set(value As PartComponentDefinition)
            partDef = value
        End Set
    End Property

    Property OriginalName As String
        Get
            Return oPartName
        End Get
        Set(value As String)
            oPartName = value
        End Set
    End Property

    Property NewName As String
        Get
            Return nPartName
        End Get
        Set(value As String)
            nPartName = value
        End Set
    End Property

    Property OriginalFullFileName As String
        Get
            Return oPartFullFileName
        End Get
        Set(value As String)
            oPartFullFileName = value
        End Set
    End Property

    'This is the full file name of the new part including path
    Property NewFullFileName As String
        Get
            Return nPartFullFileName
        End Get
        Set(value As String)
            nPartFullFileName = value
        End Set
    End Property

    Property OriginalPartNumber As String
        Get
            Return oPartNumber
        End Get
        Set(value As String)
            oPartNumber = value
        End Set
    End Property

    Property NewPartNumber As String
        Get
            Return nPartNumber
        End Get
        Set(value As String)
            nPartNumber = value
        End Set
    End Property

    'checks if the name of the part has been changed from the basic prefix and suffix
    ReadOnly Property NameOverride As Boolean
        Get
            Return nameOver
        End Get
    End Property

    ReadOnly Property ParentAssemblys As ArrayList
        Get
            Return parentAsys
        End Get
    End Property

    'override basic prefix and suffix of name
    '****THIS FUNCTION IS NOT COMPLETE!!!!!!
    Public Function OverrideName(newName As String) As Object
        nameOver = True
        nPartName = newName


        Return vbNull
    End Function

    Public Function AddParentAssembly(newAsy As AssemblyFile) As Object
        parentAsys.Add(newAsy)
        Return vbNull
    End Function

End Class
